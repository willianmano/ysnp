<?php

namespace WM\YSNP\Utils;

class HttpCodes
{
    const OK = 200; //OK Sucesso!
    const BAD_REQUEST = 400; //Bad Request A requisição possui parametro(s) inválido(s)
    const UNAUTHORIZED = 401; //Unauthorized O token de acesso não foi informado ou não possui acesso as APIs.
    const NOT_FOUND = 404; //Not Found O recurso informado no request não foi encontrado.
    const REQUEST_TOO_LARGE = 413; //Request is to Large A requisição está ultrapassando o limite permitido para o perfil do seu token de acesso.
    const UNPROCESSABLE = 422; //Unprocessable Entity A requisição possui erros de negócio.
    const TOO_MANY_REQUESTS = 429; //Too Many Requests O consumidor estourou o limite de requisições por tempo.
    const INTERNAR_SERVER_ERROR = 500; //Internal Server Error Erro não esperado, algo está quebrado na API.
}