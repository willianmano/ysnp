<?php

namespace WM\YSNP\Services\Silex;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use WM\YSNP\Utils\HttpCodes;

class BeforeMiddlewareServiceProvider implements ServiceProviderInterface
{
    public function register(Application $app)
    {
        $app->before(function(Request $request, Application $app) {
            $pathInfo = $request->getPathInfo();

            $openRoutes = [];
            if(isset($app['open_routes']) && !empty($app['open_routes'])) {
                $openRoutes = $app['open_routes'];
            }

            if(in_array($pathInfo, $openRoutes)) {
                return true;
            }

            $token = $request->headers->get('Authorization');

            $guardian = $app['guardian'];

            $validToken = $guardian->validateToken($token);

            if (!$validToken) {
                return new JsonResponse('Acesso negado', HttpCodes::UNAUTHORIZED);
            }

            $app['ClientToken'] = strval($validToken);
        });
    }

    public function boot(Application $app)
    {
    }
}
