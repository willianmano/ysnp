<?php

namespace WM\YSNP\Services\Silex;

use Silex\Application;
use Silex\ServiceProviderInterface;
use WM\YSNP\Guardian;

class GuardianServiceProvider implements ServiceProviderInterface
{
    public function register(Application $app)
    {
        $app['guardian'] = $app->share(function() use ($app) {
            return new Guardian($app['db']);
        });
    }

    public function boot(Application $app)
    {
    }
}
