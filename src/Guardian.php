<?php

namespace WM\YSNP;

//use WM\YSNP\Generators\Sha2TokenGenerator as TokenGenerator;
use WM\YSNP\Generators\JWTTokenGenerator as TokenGenerator;
use WM\YSNP\Exceptions\InvalidLoginException;
use Lcobucci\JWT\ValidationData;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class Guardian
{
    protected $conn;

    public function __construct(\PDO $conn)
    {
        $this->conn = $conn;
    }

    public function login(array $credentials)
    {
        $usuario = $this->findUserByEmail($credentials['email']);

        if (!$usuario) {
            throw new InvalidLoginException('Email e/ou senha incorreto(s).');
        }

        if (password_verify($credentials['senha'], $usuario->senha)) {
            $this->verifyIfNeedsRehash($usuario->senha, $credentials['senha'], $usuario->id);
        } else {
            throw new InvalidLoginException('Usuário e/ou senha incorreto(s).');
        }

        $usuarioData = [
            'id' => md5($usuario->id),
            'nome' => $usuario->nome,
            'email' => $usuario->email
        ];

        return $this->generateToken($usuarioData);
    }

    public function validateToken($token = null)
    {
        if (is_null($token)) {
            return false;
        }

        $token = str_replace('Bearer ', '', $token);

        $token = (new Parser())->parse($token);

        $signer = new Sha256();

        // Verifica se a chave do token corresponde com a chave da aplicacao
        if (!$token->verify($signer, 'game.api.signature')) {
            return false;
        }

        $validation = new ValidationData();
        $validation->setIssuer('http://game.api');
        $validation->setAudience('http://game.api');

        // Verifica se o token eh valido
        $isValid = $token->validate($validation);

        if (!$isValid) {
            return false;
        }

        // Verifica se o token precisa ser recriado. O tempo verificacao eh de um dia
        $validation->setCurrentTime(time() + 86400);
        $needRegenerate = !$token->validate($validation);

        if ($needRegenerate) {
            $payload = $token->getClaims();

            $usuarioData = [
                'id' => $payload['jti'],
                'nome' => $payload['nome'],
                'email' => $payload['email']
            ];

            return $this->generateToken($usuarioData);
        }

        return $token;
    }

    private function findUserByEmail($email)
    {
        $sql = "SELECT * FROM usuarios WHERE email = :email";

        $query = $this->conn->prepare($sql);
        $query->bindParam('email', $email);
        $query->execute();

        return $query->fetchObject();
    }

    private function verifyIfNeedsRehash($hash, $password, $id)
    {
        $needsRehash = password_needs_rehash($hash, PASSWORD_BCRYPT);

        if ($needsRehash) {
            $hash = password_hash($password, PASSWORD_BCRYPT);

            $sql = "UPDATE usuarios SET senha = :senha WHERE id = :id";

            $query = $this->conn->prepare($sql);
            $query->bindParam('senha', $hash);
            $query->bindParam('id', $id);
            $query->execute();
        }
    }

    public function generateToken(array $data)
    {
        return TokenGenerator::generate($data);
    }
}
