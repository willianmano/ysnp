<?php

namespace WM\YSNP\Generators;

interface TokenGeneratorInterface
{
    public static function generate(array $data = []);
}