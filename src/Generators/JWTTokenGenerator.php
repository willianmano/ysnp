<?php

namespace WM\YSNP\Generators;

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class JWTTokenGenerator implements TokenGeneratorInterface
{
    public static function generate(array $data = [])
    {
        return (new Builder())->setIssuer('http://game.api') // Configures the issuer (iss claim)
            ->setAudience('http://game.api') // Configures the audience (aud claim)
            ->setId($data['id'], true) // Configures the id (jti claim), replicating as a header item
            ->setIssuedAt(time())
            ->setExpiration(time() + 604800) // 1 semana
            ->set('nome', $data['nome'])
            ->set('email', $data['email'])
            ->sign(new Sha256(), 'game.api.signature')
            ->getToken();
    }
}
